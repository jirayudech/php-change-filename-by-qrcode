
<?php

include_once('./lib/QrReader.php');
 set_time_limit(60);
 ini_set('memory_limit', '1024M'); 

$dir = scandir('qrcodes');
$ignoredFiles = array(
	'.',
	'..',
	'.DS_Store'
);


$count = 0;
foreach($dir as $file) {

    $count = $count+1;
    if ($count > 5){
        header("Refresh:0");
    }

    if(in_array($file, $ignoredFiles)) continue;
    
    print $file;
   
    $im = imagecreatefromjpeg("qrcodes/".$file);
    $size = getimagesize("qrcodes/".$file);

    $to_crop_array = array('x' =>  $size[0]-500 , 'y' =>  1500, 'width' => 500, 'height'=> 500);

//Portrait BottomRight Work.
// $to_crop_array = array('x' =>  $size[0]-800 , 'y' => 1850, 'width' => 750, 'height'=>2000);

//Portrait Bottom 
 //$to_crop_array = array('x' =>  0, 'y' => 1900, 'width' => $size[0], 'height'=>$size[1]-1900);

    $thumb_im  = mycrop($im, $to_crop_array);
 
    imagejpeg($thumb_im, "qrcodes/thumb.jpeg");
 
    print ' --- ';
    $qrcode = new QrReader("qrcodes/thumb.jpeg");
    
    $text = $qrcode->text();
    
    if(strlen($text)>13){
        print $text;
        
        $dest = imagecreatetruecolor($size[0], $size[1]);
        imagecopy(
            $dest,
            $im,
            0,
            0,
            0,
            0,
            $size[0],
            $size[1]
        );   
        
       $output_filename = "output/".$qrcode->text().".jpg";
       if(imagejpeg($dest, $output_filename)){
           unlink("qrcodes/".$file);
       }
          
       
        
       imagedestroy($dest);
    }else{
        $dest = imagecreatetruecolor($size[0], $size[1]);
        imagecopy(
            $dest,
            $im,
            0,
            0,
            0,
            0,
            $size[0],
            $size[1]
        );   
        
        $output_filename = "error/".$file;
                

       if(imagejpeg($dest, $output_filename)){
           unlink("qrcodes/".$file);
       }
        imagedestroy($dest);               
    }
    
    imagedestroy($im);
    imagedestroy($thumb_im);
    print "<br/>";
}


function mycrop($src, array $rect)
{
    $dest = imagecreatetruecolor($rect['width'], $rect['height']);
    imagecopy(
        $dest,
        $src,
        0,
        0,
        $rect['x'],
        $rect['y'],
        $rect['width'],
        $rect['height']
    );

    return $dest;
}

?>

